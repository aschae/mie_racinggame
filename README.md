# MIE_TA1_RacingGame
## instructions
### setup
#### obligatory packages for own virt Env
package | version
------------ | -------------
Cython | 0.28.2
Kivy | 1.10.1
#### music
to play a song during the game, put an audio file named "music.TYPE" in "data/audio" and adjust the TYPE in main.py (search for "todo")
- [our recommendation](https://www.youtube.com/watch?v=l482T0yNkeo)
### start game
#### from terminal
in project's root folder:
./NAME_OF_YOUR_VIRTUAL_ENVIRONMENT/bin/python main.py
#### from IDE
run main.py
### controls
- steer left: A/"left arrow"
- steer right: D/"right arrow"

BEAT THE HIGHSCORE!

## Roadmap
### step1
- [x] display track (lines) and car (simple cube)
### step2
- [x] car can be moved in 3 lines
### step3
- [x] car  "drives"
### step4
- [x] finite number of obstacles
### step5
- [x] collision detection
### step6
- [x] score
### step7
- [x] random generation of obstacles
### cerise sur le gâteau
- [x] add sounds
- [x] add graphics (car)
- [x] add music
- [x] different speed/difficulty levels
- [x] different obstacle forms
- [ ] animations
- [ ] fluid movements
- [ ] improve graphics
- [ ] powerups (weapons)
