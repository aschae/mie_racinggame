from kivy.app import App
from kivy.uix.widget import Widget
from kivy.properties import NumericProperty, ReferenceListProperty,\
    ObjectProperty
from kivy.clock import Clock
from kivy.core.window import Window
from kivy.core.audio import SoundLoader
import random as rnd


class Obstacle(Widget):
    velocity_y = NumericProperty(0)
    lane = NumericProperty(0)
    passed = True
    garage = False

    def move(self):
        if self.lane == 0:
            x = Window.size[0] / 5 - 20
        if self.lane == 1:
            x = Window.size[0] / 2 - 20
        if self.lane == 2:
            x = Window.size[0] * 4 / 5 - 20

        self.pos = x, self.pos[1] + self.velocity_y


class Car(Widget):
    lane = NumericProperty(1)
    tireSound = SoundLoader.load('data/audio/carscreech3s.wav')

    def move(self):
        if self.lane == 0:
            x = Window.size[0]/5 - 20
        if self.lane == 1:
            x = Window.size[0]/2 - 20
        if self.lane == 2:
            x = Window.size[0]* 4 / 5 - 20
        self.pos = x, Window.size[1]/4


class RacingGame(Widget):
    music = SoundLoader.load('data/audio/music.m4a') # TODO: change filetype here
    score = NumericProperty(0)
    best = NumericProperty(0)
    last = NumericProperty(0)
    probab = NumericProperty(0)
    double = False
    garage = []
    maxDistance = 1200
    minDistance = 600
    speed = 0.001
    car = ObjectProperty(None)
    obstacle1 = ObjectProperty(None)
    obstacle2 = ObjectProperty(None)
    obstacle3 = ObjectProperty(None)
    obstacle4 = ObjectProperty(None)
    obstacles = []

    doubles = NumericProperty(0)
    randoms = NumericProperty(0)

    def reset(self):
        self.maxDistance = 1200
        self.minDistance = 600
        if self.score > self.best:
            self.best = self.score
            file = open("data/score.txt", "w")
            file.write(str(self.best))
            file.close()
        self.last = self.score
        self.score = 0
        self.speed = 0.001
        self.double = False
        self.probab = 0
        self.start_car()
        for obst in self.obstacles:
            obst.velocity_y = -4
            obst.pos = (obst.pos[0], -50)
            obst.passed = True
            obst.garage = False
        self.music.stop()
        self.music.play()

    def start_car(self):
        self.car.center = self.center
        self.car.lane = 1


    def start_obstacles(self):
        self.obstacles.append(self.obstacle1)
        self.obstacles.append(self.obstacle2)
        self.obstacles.append(self.obstacle3)
        self.obstacles.append(self.obstacle4)

        for obst in self.obstacles:
            obst.velocity_y = -4


    def updateDifficulty(self, dt):
        for obst in self.obstacles:
            obst.velocity_y -= 0.2
        self.speed = min(self.speed + 0.001, 0.06)
        self.minDistance = max(200, self.minDistance - 25)
        self.maxDistance = max(max(self.minDistance, 300), self.maxDistance-25)
        self.probab = min(60, self.probab+2)


    def update(self, dt):
        tcs = self.tex_coords
        for i in range(1, 8, 2):
            self.tex_coords[i] += 0.01+self.speed

        self.car.move()
        for obst in self.obstacles:
            if obst.garage is False:
                obst.move()

        for obst in self.obstacles:
            if (obst.y < self.car.y - 100) and obst.passed is False:
                self.score += 1
                obst.passed = True
# lane function
        for obst in self.obstacles:
            if obst.y < -75 and obst.garage is False:
                if self.double is False:
                    self.randoms += 1
                    if rnd.random() * 100 < self.probab:
                        self.doubles += 1
                        self.double = True
                if self.double is True:
                    if len(self.garage) == 1:
                        newLane = rnd.randint(0, 1)
                        lastCarPos = 0
                        for currObst in self.obstacles:
                            lastCarPos = max(lastCarPos, currObst.y + 100)
                        newDistance = max(self.height, lastCarPos) + max(self.minDistance, 350) + (
                                max(self.maxDistance, 400) - max(self.minDistance, 350)) * rnd.random()

                        newCar = self.garage.pop(0)
                        newCar.lane = newLane
                        newCar.y = newDistance
                        newCar.passed = False
                        newCar.garage = False
                        obst.lane = newLane+1
                        obst.y = newDistance
                        obst.passed = False
                        self.double = False
                    else:
                        obst.garage = True
                        self.garage.append(obst)
                else:
                    obst.passed = False
                    newLane = rnd.randint(0, 2)
                    obst.lane = newLane
                    lastCarPos = 0
                    for currObst in self.obstacles:
                        lastCarPos = max(lastCarPos, currObst.y + 100)
                    obst.y = max(self.height, lastCarPos) + self.minDistance + (
                                self.maxDistance - self.minDistance) * rnd.random()

        for obst in self.obstacles:
            if obst.lane == self.car.lane:
                if abs(self.car.y - obst.y) <= 100:
                    self.reset()


    def _keyboard_closed(self):
        self._keyboard.unbind(on_key_down=self._on_keyboard_down)
        self._keyboard = None


    def _on_keyboard_down(self, keyboard, keycode, text, modifiers):
        if keycode[1] == 'right' or keycode[1] == 'd':
            if self.car.lane < 2:
                self.car.tireSound.play()
                self.car.lane += 1
        if keycode[1] == 'left' or keycode[1] == 'a':
            if self.car.lane > 0:
                self.car.tireSound.play()
                self.car.lane -= 1
        return True


    def __init__(self):
        super(RacingGame, self).__init__()
        self._keyboard = Window.request_keyboard(self._keyboard_closed, self)
        self._keyboard.bind(on_key_down=self._on_keyboard_down)
        self.canvas.children[0].texture.wrap = 'repeat'
        file = open("data/score.txt", "r")
        self.best = int(file.readline())
        file.close()



class RacingApp(App):
    def build(self):
        game = RacingGame()
        game.start_car()
        game.start_obstacles()
        game.music.play()
        Clock.schedule_interval(game.update, 1.0 / 60.0)
        Clock.schedule_interval(game.updateDifficulty, 3.0 / 1.0)
        return game


if __name__ == '__main__':
    RacingApp().run()
